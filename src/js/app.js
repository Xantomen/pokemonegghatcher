'use strict';

var ENVIRONMENT = "DEVELOPMENT";



if(document.location.hostname != "localhost") ENVIRONMENT = "PRODUCTION";


/*var EmailInput = React.createClass({
	
	render:function() {
		
		return (
			
			<input type={this.props.input_type} className={this.props.input_validation} onChange={this.updateEmailValue} placeholder={this.props.input_placeholder}/>
		);
	},
  	updateEmailValue: function(event) {
  		
  		RequestData.emailResponder = event.target.value;
  	
  	}
	
});*/

var BootstrapButton = React.createClass({
  render: function() {
    return (
      <a {...this.props}
        href="javascript:;"
        role="button"
        className={(this.props.className || '') + ' btn'} />
    );
  }
});

var BootstrapModal = React.createClass({
	
	getInitialState: function() {
    // Start off by selecting the first board
	    return {
	      modalTitle: "SUCCESS!"
	    }
	  },
  // The following two methods are the only places we need to
  // integrate Bootstrap or jQuery with the components lifecycle methods.
  componentDidMount: function() {
    // When the component is added, turn it into a modal
    $(this.refs.root).modal({backdrop: 'static', keyboard: false, show: false});

    // Bootstrap's modal class exposes a few events for hooking into modal
    // functionality. Lets hook into one of them:
    $(this.refs.root).on('hidden.bs.modal', this.handleHidden);
  },
  componentWillUnmount: function() {
    $(this.refs.root).off('hidden.bs.modal', this.handleHidden);
  },
  close: function() {
    $(this.refs.root).modal('hide');
  },
  open: function() {
    $(this.refs.root).modal('show');
    
    this.setState({
      modalTitle: "SUCCESS!"
   	});
  },
  render: function() {
    var confirmButton = null;

    if (this.props.confirm) {
      confirmButton = (
        <BootstrapButton
          onClick={this.handleConfirm}
          className="btn-primary">
          {this.props.confirm}
        </BootstrapButton>
      );
    }
    
    return (
      <div className="modal fade" ref="root">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button
                type="button"
                className="close"
                onClick={this.handleConfirm}>
                &times;
              </button>
              <h3>{this.state.modalTitle}</h3>
            </div>
            <div className="modal-body">
              {this.props.children}
            </div>
            <div className="modal-footer">
              {confirmButton}
            </div>
          </div>
        </div>
      </div>
    );
  },
  handleConfirm: function() {
    
      this.props.onConfirm();
      
  }
  
});

var EggHatcherApp = React.createClass({

  getInitialState: function() {
    // Start off by selecting the first board
    return {
      eggClick: eggClick,
      animationClass: "",
      isAnimationFinish: true,
      emailVerificationFailure: false,
      postalcodeVerificationFailure: false
    }
  },

  render: function() {
  	     	 
  	var header_content = "";
  	
  	var subtitle_content = "";
  	
  	var animation_content = "";
  	
  	var egg_effects = "";
  	
  	var egg = "";
  	
  	var pokedex_container = "";
  	
  	var request_form = "";
  	
  	var background_color_animations = "";
  	
  	var impressum = "";
  	
  	var modal = null;
            
    modal = (
      <BootstrapModal
        ref="modal"
        confirm="OK"
        onConfirm={this.handleConfirm}
        title="Success!">
        	"Our closest Hatcher will contact you soon!"
        	<br/>
	
      </BootstrapModal>
    );
  	  
  	  
  	if(this.state.eggClick == 0)
  	{
  		
  		background_color_animations = (
  			<div>
		      <div className="background1 make-visible"></div>
	      	</div>
	    );
	    	    
	    	    
	    egg = (
	    	
	    	<div id="egg" onClick={this.eggMove} className={"egg_animation_state" + (this.state.eggClick + 1) + " wiggle-animation"}></div>
	    );
  	} 
  	if(this.state.eggClick == 1)
  	{
  		
  		background_color_animations = (
  			<div>
		      <div className="background1 make-visible"></div>
	      	</div>
	    );
	    
	    egg = (
	    	
	    	<div id="egg" onClick={this.eggMove} className={"egg_animation_state" + (this.state.eggClick + 1) + " " + this.state.animationClass}></div>
	    );
  	}
  	else if(this.state.eggClick == 2)
  	{
  		
  		background_color_animations = (
  			<div>
  			  <div className="background1"></div>
		      <div className="background2 make-visible2"></div>
	      	</div>
	    );
	    
	    egg = (
	    	
	    	<div id="egg" onClick={this.eggMove} className={"egg_animation_state" + (this.state.eggClick + 1) + " " + this.state.animationClass}></div>
	    );
  	}
  	else if(this.state.eggClick == 3)
  	{
  		
  		background_color_animations = (
  			<div>
  			  <div className="background2"></div>
		      <div className="background3 make-visible3"></div>
	      	</div>
	    );
	    
	    egg = (
	    	
	    	<div id="egg" onClick={this.eggMove} className={"egg_animation_state" + (this.state.eggClick + 1) + " " + this.state.animationClass}></div>
	    );
  	}
  	else if(this.state.eggClick == 4) /* Completed Egg Ecclosion*/
  	{

		if (!localStorage.eggClick) {
		    localStorage.eggClick = 4;
		}

		background_color_animations = (
  			<div>
  			  <div className="background3"></div>
		      <div className="background4 make-visible4"></div>
	      	</div>
	    );
	    
	    egg = (
	    	
	    	<div id="egg" onClick={this.eggMove} className={"egg_animation_state" + (this.state.eggClick + 1) + " " + this.state.animationClass}></div>
	    );

		header_content = (
  			<div>
		      <div id="egg_hatchers_title" className="slide-up-from-egg-title"></div>
		      
	      	</div>
	    );
	    
	    subtitle_content = (
	    	<div id="subtitle" className="slide-up-from-egg-subtitle"></div>
	    );

  		animation_content = (
  			<div>
	      <div id="egg_bits_right" className="dissapear-right-animation"></div>
	      <div id="egg_bits_left" className="dissapear-left-animation"></div>
	     
	      	</div>
	    );
	    
	    egg_effects = (
  			<div>
	      		<span className="particle_generator circles"></span>
	      	</div>
	    );
	    
	    pokedex_container = (
	    	
	    	<div id="pokedex_container" className="slide-in-from-left">
				<div id="lights_row">
				
				  <div id="lights_component_background"></div>
				  <div id="lights_component_background2"></div>
				  <div id="lights_component">
					  <div id="blue_light_container">
					  	<div id="blue_light"></div>
					  </div>
				      
				       <div id="red_light" className="small_light"></div>
				       <div id="yellow_light" className="small_light"></div>
				       <div id="green_light" className="small_light"></div>
				    </div>
				</div>
				<div id="body_row">
					<div id="screen_display">
						<div className="h2-text-size">Benefits of hatching Eggs:</div>
  						<div className="h3-text-size">- Discover rarer, higher level and special region Pokemon.</div>
  						<div className="h3-text-size">- Get lots of XP Points, Stardust, and up to 30 Candy per Egg.</div>
  						<div className="h2-text-size">What we do:</div>
  						<div className="h3-text-size">- We hatch your Pokemon GO Eggs while you are busy.</div>
  						<div className="h3-text-size">- We catch Pokemon along the way and fill your inventory with useful items.</div>
  						<div className="h2-text-size">In addition:</div>
  						<div className="h3-text-size">- We level up your character.</div>
  						<div className="h3-text-size">- We tell you which of your new Pokemon are worth leveling up.</div>
					</div>
				</div>
			</div>
	    );
	    
	    impressum = (
	    	
	    	<div id="impressum" className="h3-text-size">
	    		Site owned and operated by Kathartik Experiences UG	- Not affiliated with Niantic Inc. or Pokemon GO.
	    	</div>
	    	
	    );
	    
	    var email_input = (<input type="email" id="email" placeholder="Email" className="input-text-fields col-xs-7 col-xs-offset-1" />);
	    var postcode_input = (<input type="number" id="postal_code" placeholder="PLZ" className="input-text-fields col-xs-3" />);
	    
	    if(this.state.emailVerificationFailure)
	    {
	    	var email_input = (<input type="email" id="email" placeholder="Email" className="input-text-fields col-xs-7 col-xs-offset-1 input-failure"/>);	
	    }
	    if(this.state.postalcodeVerificationFailure)
	    {
	    	var postcode_input = (<input type="number" id="postal_code" placeholder="PLZ" className="input-text-fields col-xs-3 input-failure"/>);
	    }
	    
	    request_form = (
  			<div id="request_form_container" className="slide-in-from-right">
				<div id="request_form">
				
					<div className="h2-text-size">Get a Hatcher!</div>
					<div className="h3-text-size">Safe and practical.<br/>We don't need your device or account details!</div>
					<div className="h3-text-size">Write up your email and postal code and our closest Hatcher will contact you to walk your PokeEggs.</div>
					<div className="h3-text-size">(Maybe even Margaret Hatcher)</div>
					{/*<img id="pokemon_player" src="res/img/pokemongo_person" className="img-responsive" alt="pokemon player"/>*/}
					<div className = "row">	
						{email_input}
						{postcode_input}
					</div>
					
					<div id="submit_button" className="btn btn-primary h3-text-size" onClick={this.saveFormToDatabase}>	Hatcher, I choose You!</div>
					
				
				</div>
			</div>
	    );
	    	 
	   	
	    setTimeout(function(){
  			 
  			 spawnCircles();
  		
  		}, 1000);	 
  		
  	}   
  	 
  	
  	
  	function spawnCircles() {
  		
  		
  		
	   $.each($(".particle_generator.circles"), function(){
	   	
	   		console.log($(this));
	   	
	      var circlecount = ($(this).width()/50)*10;
	      for(var i = 0; i <= circlecount; i++) {
	         var size = ($.rnd(40,80)/10);
	         $(this).append('<span class="particle" style="top:' + $.rnd(20,80) + '%; left:' + $.rnd(0,95) + '%;width:' + size + 'px; height:' + size + 'px;animation-delay: ' + ($.rnd(0,30)/10) + 's;"></span>');
	      }
	   });
	}
		
	jQuery.rnd = function(m,n) {
	      m = parseInt(m);
	      n = parseInt(n);
	      return Math.floor( Math.random() * (n - m + 1) ) + m;
	}
  	     	    
    return (
		
		<div id="background_container">
		
			{background_color_animations}
		
  		<div id="container_all" className="container-fluid">	
  		
  			<div id="preload_div">
	  			<div id="preload-01"></div>
	  			<div id="preload-02"></div>
	  			<div id="preload-03"></div>
	  			<div id="preload-04"></div>
	  			<div id="preload-05"></div>
	  			<div id="preload-06"></div>
	  			<div id="preload-07"></div>
	  			<div id="preload-08"></div>
	  		</div>
  		  		
    		<div id="pokemon_egg_hatchers_container" className="h1 center-block row">
    		
    			<div id="egg_hatch_container" className="col-xs-6 col-xs-offset-3 row">
    				
    				
    				{header_content}
    				
    				{subtitle_content}
    				
    				
    				<div id="egg_hatch" className="col-xs-8 col-xs-offset-2">
    					
    					
    					{animation_content}
    					
    					{egg_effects}
    					 
    					{egg}
    						
    				
    					
    					
    					
		  			</div>
		  			
		  			
  				</div>
  				
  				{pokedex_container}
  				
  				{request_form}
  				
  				{impressum}
  				
  				{modal}
  				
			</div>	
			
			
		</div>
		
		</div>
  		
		
    );
  },
  eggMove: function() {

	if(this.state.eggClick < 4)
	{

		if(this.state.isAnimationFinish)
		{
			
			this.setState({
		      eggClick: this.state.eggClick + 1,
		      animationClass: "swing-animation",
		      isAnimationFinish: false
		    }, timeoutAnimationClass(this));
			
		}
	}
	else if(this.state.eggClick == 5)
	{
		
	}
	
	
  	function timeoutAnimationClass(object_ref) {
  		
  		setTimeout(function(){
  			 
  			 object_ref.setState({
	
			      animationClass: "",
			      isAnimationFinish: true
		
			  	});
  		
  		}, 580);
  		
	  	console.log("Clicks:" +object_ref.state.eggClick);
	  	
	  	
	}
  	//console.log("Number of clicks: "+this.state.eggClick);
  	
  },
  saveFormToDatabase: function(event) {
 
 	var email = $("#email").val();
 	var postal_code = $("#postal_code").val();
 	var country = "DE";
 	var city = "berlin";
 	
 	var validation = true;
 	
 	if(!validateEmail(email))
 	{
 		validation = false;
 		this.setState({
	      emailVerificationFailure: true
	    });
 	}
 	else
 	{
 		this.setState({
	      emailVerificationFailure: false
	    });
 	}
 	if(postal_code == "")
 	{
 		validation = false;
 		this.setState({
	      postalcodeVerificationFailure: true
	    });
 	}
 	else
 	{
 		this.setState({
	      postalcodeVerificationFailure: false
	    });
 	}
 	
 	if(validation)
 	{	
 		RequestData.email = email;
 		RequestData.postal_code = postal_code;
 		RequestData.country = country;
 		RequestData.city = city;
 		
 		saveFormToDatabase();
 		
 		this.refs.modal.open();
 		
   
 	}
 	
  },
  handleConfirm: function() {
    this.refs.modal.close();
  }
  
});


var RequestData = {};

var eggClick = 0;
  
if(localStorage.eggClick == 4)
{
	eggClick = 4;
}

initiateApp();

function restart_url() {
	window.location.hash = window.location.hash.split("#")[0];
}

/*function changeUrlToId(question_id) {
	window.location.hash = window.location.hash.split("#")[0]+"action=request_question&question_id="+question_id;
}*/

function initiateApp() {
		
	var action = getURLParameter("action");
	
	switch(action)
	{
		
		default:
														
			ReactDOM.render(<EggHatcherApp/>, document.getElementById('pokemonegghatchers_react'));
		
			break;
		
			
	}
	
	if(ENVIRONMENT == "PRODUCTION")
	{
		mixpanel.track("web_started", {
	        "action_type": action
	    });	
	    
	    mixpanel.identify(mixpanel.get_distinct_id());
			
		mixpanel.people.set({
		    "$last_login": new Date()         // properties can be dates...
		});
	}
	
	
}

function saveFormToDatabase() {
		
	var finalRequestData = RequestData;
	
	
	if(validateEmail(finalRequestData.email) && finalRequestData.postal_code != "")
	{
		
		console.log(finalRequestData);
		
		$.ajax({  
		    type: "POST",  
			url: "includes/save_request.php",
			data: finalRequestData,       
			success: function(response_string){ 	       
			   
			   console.log(response_string);
			   
			   if(response_string.indexOf("ERROR:") == -1)
				{
					console.log("saved_full");
					
					$.ajax({  
					    type: "POST",  
						url: "includes/send_email.php",      
						success: function(response_string){ 	       
						   
						   console.log(response_string);
						   
						   if(response_string.indexOf("Error:") == -1)
							{
								console.log("sent_email");
								
			 					return "saved_full";
							}
							else {
								console.log("send_error");
								return "send_error";
							}
						},
						error: function() {
							
							console.log("send_error");
							return "send_error";
						}
					});
					
				}
				else {
					console.log("save_error");
					return "save_error";
				}
			},
			error: function() {
				
				console.log("save_error");
				return "save_error";
			}
		});
		
	} 
	else
	{
		console.log("save_error");
		return "invalid_email";
	}
	
	
	if(validateEmail(finalRequestData.email))
	{
		if(ENVIRONMENT == "PRODUCTION")
		{
			mixpanel.alias(finalRequestData.email);
			
			mixpanel.people.set({
			    "$email": finalRequestData.email,    // only special properties need the $
			    "$last_login": new Date()         // properties can be dates...
			});
		}
	}
	
	if(ENVIRONMENT == "PRODUCTION")
	{
		mixpanel.track("Submission", {
	        "postal_code": finalRequestData.postal_code,
	        "email ": finalRequestData.email,
	     
	    });
   }
	
	
}

function transformEpochTimestampToDate(epoch_timestamp) {
	
	var myDate = new Date(epoch_timestamp*1000);
	
	return myDate;
}

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function getURLParameter(sParam)
{
	if(window.location.href.indexOf("#")>-1)
	{
		var sPageURL = window.location.href.split("#")[1];
    
    
    	var sURLVariables = sPageURL.split('&');
	    for (var i = 0; i < sURLVariables.length; i++) 
	    {
	        var sParameterName = sURLVariables[i].split('=');
	        if (sParameterName[0] == sParam) 
	        {
	            return sParameterName[1];
	        }
	    }
	}

}